package com.refactory;

import java.util.Arrays;

public class Sorting {

    void sortAngka() {
        int[] angka = {4, 9, 7, 5, 8, 9, 3};
        int temp = 0;
        int jumlahSwap = 0;

        for(int i=0; i<angka.length; i++) {
            for(int j=i+1; j<angka.length; j++) {
                if(angka[i] > angka[j]) {
                    temp = angka[i];
                    angka[i] = angka[j];
                    angka[j] = temp;
                    jumlahSwap++;
                    System.out.println("[" + angka[i] + ", " +angka[j] + "]" + "->" + Arrays.toString(angka));
                }
            }
        }
        System.out.println("Jumlah Swap : " + jumlahSwap);
    }
    
}
